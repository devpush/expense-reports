@extends('layouts.app')

@section('content') 
<div class = "container">


<div class = "row">
    <div class = "col">
        <div class = "row">
            <div class = "col">
                <h1>Edit a Reports {{ $report->id }}</h1>
            </div>
        </div>
        <div class = "row">
            <div class = "col">
                <a  
                    class = "btn btn-secondary"
                    href = "/expense_reports">
                    Back
                </a>
            </div>
        </div>
        <div class = "row">
            <div class = "col">
                <form  action = "/expense_reports/{{ $report->id }}"  method = "POST">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for = "title"> Title </label>
                        <input 
                            required
                            id = "title"  
                            type = "text"
                            name = "title"
                            value = "{{ $report->title }}"
                            class = "form-control" 
                            placeholder = "Type a title" 
                        >
                        <button
                            class = "btn btn-primary"
                            type  = "submit"
                        >
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>

@endsection