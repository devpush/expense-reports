@extends('layouts.app')

@section('content') 
<div class = "container">


<div class = "row">
    <div class = "col">
        <div class = "row mt-3">
            <div class = "col">
                <h1>Reports</h1>
            </div>
        </div>
        <div class = "row mt-3">
            <div class = "col">
                <a  
                    class = "btn btn-primary"
                    href = "expense_reports/create">
                    Create a new report
                </a>
            </div>
        </div>
        <div class = "row mt-3">
            <div class = "col">
                <table class = "table">

                    @if(count($expenseReports)>0)
                        @foreach($expenseReports as $expenseReport)
                            <tr>
                                <td> <a href = "/expense_reports/{{ $expenseReport->id }}">{{ $expenseReport->title  }}</a> </td>
                                <td> <a href = "/expense_reports/{{ $expenseReport->id }}/edit"> Edit </a> </td>
                                <td> 
                                    <a 
                                        href = "#"
                                        type = "button" 
                                        data-toggle = "modal" 
                                        class = "btn btn-danger" 
                                        data-target = "#modalDelete"
                                        onclick = "storageReportId('{{ $expenseReport->id }}')"
                                    >
                                        Delete
                                    </a>    
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td> You dont have reports </td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
        <div id = "alert"></div>
    </div>
</div>

<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDeleteLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDeleteLabel">Delete report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure delete report?
        <input type="text" hidden value = "" id = "reportIdToDelete">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" onclick = "deleteReport();" >Delete</button>
      </div>
    </div>
  </div>
</div>

<script>
    
    const modalDeleteReport = document.getElementById('modalDelete');
    modalDeleteReport.addEventListener('shown.bs.modal', function () {});

    const storageReportId = reportId => document.getElementById('reportIdToDelete').value = reportId;

    const deleteReport =  async () => {

        const reportId = document.getElementById('reportIdToDelete').value;

        fetch('/expense_reports/' + reportId, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },
        })
        .then(response => { 
            response.status == 200 ? alert(0, 'Report deleted!')  : alert(2, 'Report not found!');  
            setTimeout(()=>{
                location.reload();
            }, 2000);
        })
        .catch(error => console.error('Error al eliminar el informe:', error));
    }

    /**
     * 
     * @Type of alert by category 0: success, 1: warning , 2: danger 
     */
    const alert = (type, msg) => {

        const alert = document.getElementById('alert');
        const types = [ "success", "warning", "danger" ];

        alert.className = 'alert alert-'+types[type];
        alert.role = 'alert';
        alert.textContent = msg;
    }

</script>
</div>

@endsection