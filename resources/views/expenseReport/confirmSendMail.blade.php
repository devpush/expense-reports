@extends('layouts.app')

@section('content') 
<div class = "container">


<div class = "row">
    <div class = "col">
        <div class = "row">
            <div class = "col">
                <h1>Send Reports</h1>
            </div>
            @if($errors->any())
                <div class = "alert alert-danger">

                    <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{ error }}
                        </li>
                    @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class = "row">
            <div class = "col">
                <a  
                    class = "btn btn-secondary"
                    href = "/expense_reports">
                    Back
                </a>
            </div>
        </div>
        <div class = "row">
            <div class = "col">
                <form  action = "/expense_reports/{{ $report->id }}/sendMail"  method = "POST">
                    @csrf
                    <div class="form-group">
                        <label for = "email"> Email </label>
                        <input 
                            required
                            id = "email"  
                            type = "text"
                            name = "email"
                            class = "form-control" 
                            placeholder = "Type a email" 
                        >
                        <button
                            class = "btn btn-primary"
                            type  = "submit"
                        >
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>

@endsection