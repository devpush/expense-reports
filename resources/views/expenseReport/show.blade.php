@extends('layouts.app')

@section('content') 
<div class = "container">

<div class = "row">
    <div class = "col">
        <div class = "row">
            <div class = "col">
                <h1>Report: {{ $report->title }}</h1>
            </div>
        </div>
        <div class = "row">
            <div class = "col">
                <a  
                    class = "btn btn-secondary"
                    href = "/expense_reports">
                    Back
                </a>
            </div>
        </div>
        <div class = "row">
            <div class = "col">
                <a  
                    class = "btn btn-primary"
                    href = "/expense_reports/{{ $report->id }}/confirmSendMail">
                    Send Mail
                </a>
            </div>
        </div>
        <div class = "row">
            <div class = "col">
                <h2>Details...</h2>
                <table class = "table">
                    @foreach($report->expenses as $expense)
                        <tr>
                            <td> {{ $expense->description }} </td>
                            <td> {{ $expense->amount }} </td>
                            <td> {{ $expense->created_at }} </td>
                        </tr>
                    @endforeach 
                </table>
            </div>
        </div>
        <div class = "row">
            <div class = "col">
                <a 
                    class = "btn btn-primary"
                    href = "/expense_reports/ {{ $report->id }}/expenses/create"
                >
                    New expense
                </a>
            </div>
        </div>
    </div>
</div>

</div>

@endsection