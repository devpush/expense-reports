@extends('layouts.app')

@section('content') 
<div class = "container">


<div class = "row">
    <div class = "col">
        <div class = "row">
            <div class = "col">
                <h1>Create Reports</h1>
            </div>
            @if($errors->any())
                <div class = "alert alert-danger">

                    <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{ error }}
                        </li>
                    @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class = "row">
            <div class = "col">
                <a  
                    class = "btn btn-secondary"
                    href = "/expense_reports">
                    Back
                </a>
            </div>
        </div>
        <div class = "row">
            <div class = "col">
                <form  action = "/expense_reports"  method = "POST">
                    @csrf
                    <div class="form-group">
                        <label for = "title"> Title </label>
                        <input 
                            required
                            id = "title"  
                            type = "text"
                            name = "title"
                            class = "form-control" 
                            placeholder = "Type a title" 
                        >
                        <button
                            class = "btn btn-primary"
                            type  = "submit"
                        >
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>

@endsection