<div class = "row">
    <div class = "col m-3">
        <h1>Expense Report {{ $report->id }} : {{ $report->title }}</h1>
    </div>
</div>
<div class = "row">
    <div class = "col">
        <h1>Expenses</h1>
        <table class = "table">
            @foreach($report->expenses as $expense)
                <tr>
                    <td> {{ $expense->description }} </td>
                    <td> {{ $expense->amount }} </td>
                    <td> {{ $expense->created_at }} </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>