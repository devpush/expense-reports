@extends('layouts.app')

@section('content') 
<div class = "container">


<div class = "row">
    <div class = "col">
        <div class = "row">
            <div class = "col">
                <h1>New expense</h1>
            </div>
            @if($errors->any())
                <div class = "alert alert-danger">

                    <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{ $error }}
                        </li>
                    @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class = "row">
            <div class = "col">
                <a  
                    class = "btn btn-secondary"
                    href = "/expense_reports/{{ $report->id }}">
                    Back
                </a>
            </div>
        </div>
        <div class = "row">
            <div class = "col">
                <form  
                    method = "POST"
                    action = "/expense_reports/{{ $report->id}}/expenses"  

                >
                    @csrf
                    <div class="form-group">
                        <label for = "description"> Description </label>
                        <input 
                            required
                            id = "description"  
                            type = "text"
                            name = "description"
                            class = "form-control" 
                            placeholder = "Type a description" 
                        >
                        <label for = "amount"> Amount </label>
                        <input 
                            required
                            id = "amount"  
                            type = "number"
                            name = "amount"
                            class = "form-control" 
                            placeholder = "Type a amount" 
                        >
                        <button
                            class = "btn btn-primary"
                            type  = "submit"
                        >
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>

@endsection