<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Mail\SummaryReport;
use App\Models\ExpenseReport;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ExpenseReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $user_id = 0;

        if(Auth::check())
            $user_id = Auth::id();

        $user = User::find($user_id);

        return view('expenseReport.index', [
            "expenseReports" => $user->expenseReports
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('expenseReport.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title' => 'string|required|min:3'
        ]);

        $expensReport = new ExpenseReport();
        $expensReport->title = $request->get('title');
        $expensReport->user_id = Auth::id();
        $expensReport->save();

        return redirect('/expense_reports');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $report = ExpenseReport::findOrFail($id);

        return view("expenseReport.show", [
            "report" => $report
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $report = ExpenseReport::findOrFail($id);

        return view("expenseReport.edit", [
            "report" => $report
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $request->validate([
            'title' => 'string|required|min:3'
        ]);

        $expensReport = ExpenseReport::findOrFail($id);
        $expensReport->title = $request->get('title');
        $expensReport->user_id = Auth::id();
        $expensReport->save();

        return redirect('/expense_reports');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $report = ExpenseReport::findOrFail($id);

        if(!$report) 
            return response()->json(['message' => 'Not found'], 404);

        $report->delete();

        return response()->json(['message' => "Deleted report $report->id"], 200);
    }

    public function confirmSendMail($id)
    {
        $report = ExpenseReport::findOrFail($id);
        
        return view('expenseReport.confirmSendMail', [
            'report' => $report
        ]);

    }

    public function sendMail(Request $request, $id)
    {
        $report = ExpenseReport::findOrFail($id);
        Mail::to($request->get('email'))->send(new SummaryReport($report));

        return redirect('/expense_reports/'.$id);
    }
}
