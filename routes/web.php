<?php

use App\Http\Controllers\ExpenseController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ExpenseReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () { return view('welcome'); });
Route::resource('/expense_reports', ExpenseReportController::class);
Route::get('/expense_reports/{id}/confirmSendMail',  [ ExpenseReportController::class, 'confirmSendMail']);
Route::post('/expense_reports/{id}/sendMail',  [ ExpenseReportController::class, 'sendMail']);

Route::get('/expense_reports/{expense_report}/expenses/create',  [ ExpenseController::class, 'create']);
Route::post('/expense_reports/{expense_report}/expenses',  [ ExpenseController::class, 'store']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
